package rate_test

import (
	"log"
	"math"
	"testing"
	"time"

	"bitbucket.org/terirem/ratelimiter"
)

type testCase struct {
	inDelay  time.Duration
	outDelay int
}

var (
	testCases = []testCase{
		{inDelay: time.Duration(0), outDelay: 0},
		{inDelay: time.Duration(0), outDelay: 10},
		{inDelay: time.Duration(0), outDelay: 20},
	}
)

func mockTimeNow() time.Time {
	tmock, err := time.Parse("Jan 31, 2020 at 10:00:00.000", "Jan 31, 2020 at 10:00:00.000")
	if err != nil {
		log.Fatal("wrong init time")
	}
	return tmock
}

func TestRate(t *testing.T) {
	l := ratelimiter.NewTimeLimiter(time.Minute*1, 6, mockTimeNow)
	for i, cs := range testCases {
		time.Sleep(cs.inDelay)
		out := l.Delay()
		if cs.outDelay != round(out.Seconds()) {
			t.Errorf("case # %d  expect %v got %v", i, cs.outDelay, out)
		}
	}

}

func round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}
