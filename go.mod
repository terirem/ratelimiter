module bitbucket.org/terirem/ratelimiter

go 1.15

require (
	github.com/pkg/errors v0.9.1
	github.com/prometheus/common v0.15.0
)
