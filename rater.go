package ratelimiter

import (
	"time"
)

const timeZero = time.Duration(0)

type doer func()

// Limiter perfoms a limits with different options
type TimeLimiter interface {
	// Delay is a period to wait for start task
	Delay() time.Duration
}

type RateLimiter interface {
	Aquire() (doer, error)
	Release(doer) error
}

type Limiter struct {
	TimeLimiter
	RateLimiter
}

type config struct {
	period    time.Duration
	numPeriod int
	numTotal  int
}

// New returns a new limiter
func New(d doer, opts ...Option) (l Limiter) {
	c := makeConfig(opts)
	l.RateLimiter = NewRateLimiter(d, c.numTotal)
	l.TimeLimiter = NewTimeLimiter(c.period, c.numPeriod, time.Now)

	return
}

func makeConfig(opts []Option) config {
	c := config{
		period:   time.Duration(0),
		numTotal: 1,
	}

	for _, opt := range opts {
		opt.modify(&c)
	}

	return c
}

func (l *Limiter) Do() error {
	d, err := l.Aquire()
	if err != nil {
		return err
	}

	tm := time.NewTimer(l.Delay())

	<-tm.C

	d()

	return l.Release(d)
}
