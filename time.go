package ratelimiter

import (
	"sync"
	"time"

	"github.com/pkg/errors"
)

var ErrorNotImplemented = errors.New("not implemented")

type TimeNow = func() time.Time

type timeLimiter struct {
	sync.Mutex
	now      TimeNow
	perTask  time.Duration
	timeMark time.Time
}

func NewTimeLimiter(per time.Duration, num int, v TimeNow) *timeLimiter {
	return &timeLimiter{
		now:     time.Now,
		perTask: time.Duration(int64(per) / int64(num)),
	}
}

func (t *timeLimiter) Delay() time.Duration {
	t.Lock()
	defer t.Unlock()

	now := t.now()

	dly := t.timeMark.Add(t.perTask).Sub(now)

	if dly > 0 {
		t.timeMark = now.Add(dly)
	} else {
		t.timeMark = now
	}

	return time.Duration(t.timeMark.Sub(now))
}

type timeZeroLimiter struct{}

func (t *timeZeroLimiter) Delay() time.Time { return time.Now() }

func NewZeroTimeLimiter() *timeZeroLimiter { return &timeZeroLimiter{} }
