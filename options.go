package ratelimiter

import "time"

// Option configures a Limiter.
type Option interface {
	modify(*config)
}

type timeOption struct {
	period time.Duration
	num    int
}

func (t timeOption) modify(c *config) {
	c.period = t.period
	c.numPeriod = t.num
}

// WithTime set time rater
func WithTime(period time.Duration, num int) Option {
	return timeOption{period: period, num: num}
}

// WithZeroTime set zero time rater
func WithZeroTime() Option {
	return timeOption{}
}

type rateOption int

// With Rate set num of avalible doers
func WithRate(num int) Option {
	return rateOption(num)
}

func (r rateOption) modify(c *config) {
	c.numTotal = int(r)
}
