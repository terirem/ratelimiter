package ratelimiter

import (
	"time"

	"github.com/pkg/errors"
)

var ErrClosedChannel = errors.New("channel closed suddenly")

type rateLimiter struct {
	pool chan doer
}

func NewRateLimiter(item doer, num int) *rateLimiter {
	r := &rateLimiter{
		pool: make(chan doer, num),
	}

	for i := 0; i < num; i++ {
		if err := r.Release(item); err != nil {
			return nil
		}
	}
	return r
}

func (r *rateLimiter) Delay() time.Time { return time.Now() }
func (r *rateLimiter) Aquire() (d doer, err error) {
	d, ok := <-r.pool
	if !ok {
		return nil, ErrClosedChannel
	}

	return d, nil

}
func (r *rateLimiter) Release(d doer) (err error) {
	defer func() {
		if rec := recover(); rec != nil {
			err = ErrClosedChannel
		}
	}()

	r.pool <- d

	return nil
}
