package main

import (
	"math/rand"
	"sync"
	"time"

	"bitbucket.org/terirem/ratelimiter"
	"github.com/prometheus/common/log"
)

const (
	rangeLower   = 300
	rangeUpper   = 800
	numPerMinute = 15
	loops        = 10
)

func main() {
	doer := func() {
		rand.Seed(time.Now().Unix())

		time.Sleep(time.Duration(time.Millisecond * time.Duration(rangeLower+rand.Intn(rangeUpper-rangeLower+1))))
		println("do it")
	}

	rl := ratelimiter.New(doer, ratelimiter.WithRate(5), ratelimiter.WithTime(time.Duration(time.Minute), numPerMinute))

	wg := &sync.WaitGroup{}

	for i := 0; i < loops; i++ {
		wg.Add(1)
		log.NewErrorLogger().Printf("task # %d is about to start", i)
		go func(w *sync.WaitGroup) {
			if err := rl.Do(); err != nil {
				log.Errorf("error in routine %s", err.Error())
			}
			wg.Done()
		}(wg)
	}

	wg.Wait()
}
